package com.classpath.inventorymicroservice.service;

import com.classpath.inventorymicroservice.model.Order;
import com.classpath.inventorymicroservice.model.OrderEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
public class OrderProcessor {


    @KafkaListener(groupId = "inventory-microservice", topics = "orders-topic")
    public void processOrder(String orderEvent){
        log.info("processing the order event :: {}", orderEvent);
    }
}
