package com.classpath.inventorymicroservice.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/inventory")
@Slf4j
public class InventoryController {

    private static int counter  = 1000;

    @PostMapping
    public int updateQty(){
      log.info("Came inside the updatedQty method :: ");
      return  counter--;
    }

    @GetMapping
    public int getQty(){
        return counter;
    }
}
