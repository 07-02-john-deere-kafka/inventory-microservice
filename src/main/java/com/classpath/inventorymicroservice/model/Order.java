package com.classpath.inventorymicroservice.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import java.time.LocalDate;

@Data
@NoArgsConstructor
public class Order {
    private Long id;
    private String customerName;
    private String email;
    private LocalDate date;
    private double price;
}
